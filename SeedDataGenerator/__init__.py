from SeedDataGenerator.config import DevelopmentConfig
import pyodbc
import os
import sys


os.environ["MSSQLCONFIG"] = "DEVELOPMENT"

configObj = ""
if os.getenv("MSSQLCONFIG") == "DEVELOPMENT":
    print("dev")
    configObj = config.DevelopmentConfig()
elif os.getenv("MSSQLCONFIG") == "PRODUCTION":
    print("prod")
    configObj = config.ProductionConfig
else:
    print("There is no config")
    sys.exit(1)


conn = pyodbc.connect(configObj.mssqlConfig, autocommit=True)
cursor = conn.cursor()
