from SeedDataGenerator.models.CreateDatabase import CreateDatabase
from SeedDataGenerator.service.DatabaseService import DatabaseService
import pyodbc

if __name__ == "__main__":

        create = CreateDatabase()
        create.createDatabase()
        create.createTables()

        # Relational Database
        dataService = DatabaseService()
        dataService.devUVMAssessements()
        dataService.devUVMVulnerabilites()
        dataService.devUVMAssessmentTargets()
        dataService.devUVMFindings()
        dataService.devUVMVulnerabilityFixletNexus()
        dataService.devUVMAssessementFindingNexus()
        dataService.devUVMAssessementVulnerabilitesNexus()
        dataService.devUVMSchema()

        # Fact Tables
        dataService.devUVMAggregationsDiscrepencies()
        dataService.devUVMAggregationsFindings()
        dataService.devUVMAggregationsVulnerabilites()





