from SeedDataGenerator import conn, cursor


class CreateDatabase:

    def createDatabase(self):
        query = '''
drop database test_db

CREATE DATABASE [test_db]
'''
        cursor.execute(query)
        conn.commit()

    def createTables(self):
        with open(r".\..\ddl_scripts.txt", "r") as file:
            query = file.read()
        cursor.execute(query)
        conn.commit()
