
from SeedDataGenerator import  cursor


class GetFromDatabase:

    def getDataUVMAssessment(self):
        query = '''select assessment_id from UVM.assessments'''
        cursor.execute(query)
        return cursor.fetchall()

    def getDataUVMAssessmentTargets(self):
        query = '''select computer_id from UVM.assessment_targets'''
        cursor.execute(query)
        return cursor.fetchall()

    def getDataUVMVlnerabilites(self):
        query = '''select cve from [UVM].[vulnerabilities] '''
        cursor.execute(query)
        return cursor.fetchall()

    def getDataUVMFindings(self):
        query = '''select computer_id from UVM.findings'''
        cursor.execute(query)
        return cursor.fetchall()
