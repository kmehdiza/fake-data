from SeedDataGenerator import  conn, cursor




class InsertDatabase:

    def insertUVM_Assessements(self, data):
        query = ''' insert into [UVM].[assessments] values(?,?,?,?,?,?)'''
        values = (data["AssessmentID"], data["Title"], data["ScanType"], data["DatePerformed"], data["PerformedBy"],
                  data["duration"])
        cursor.execute(query, values)
        conn.commit()

    def insertUVM_Vulnerabilites(self, data):
        query = '''insert into [UVM].[vulnerabilities] 
        values(?,?,?,?,?,?,?,?,?,?,?,?,?)'''
        values = (data["CVE"], data["Title"], data["Type"], data["Severity"], data["CVSS_base"], data["CVSS_temporal"],
                  data["CVSS3_base"],
                  data["CVSS3_temporal"], data["Impact"], data["Solution"], data["Category"],
                  data["Description"], data["DatePublished"])
        cursor.execute(query, values)
        conn.commit()

    def insertUVMAssessmentTargets(self, data):
        query = '''insert into [UVM].[assessment_targets] values(?,?)'''
        values = (data["AssessmentID"], data["ComputerID"])
        cursor.execute(query, values)
        conn.commit()

    def insertUVMFindings(self, data):
        query = '''insert into [UVM].[findings] values(?,?,?,?)'''
        values = (data['ComputerID'], data['CVE'], data['FirstDetected'], data['LastDetected'])
        cursor.execute(query, values)
        conn.commit()

    def insertUVMVulnerabilityFixletNexus(self, data):
        query = '''insert into UVM.vulnerability_fixlet_nexus values(?,?,?)'''
        values = (data['CVE'], data['SiteName'], data['FixletID'])
        cursor.execute(query, values)
        conn.commit()

    def insertUVMAssessementFindingNexus(self, data):
        query = '''insert into UVM.[assessments_findings_nexus] values(?,?)'''
        values = (data["AssessmentID"], data["ComputerID"])
        cursor.execute(query, values)
        conn.commit()

    def insertUVMAssessementVulnerabilitesNexus(self, data):
        query = '''insert into UVM.assessments_vulnerabilities_nexus values(?,?)'''
        values = (data['CVE'], data['AssessmentID'])
        cursor.execute(query, values)
        conn.commit()

    def insertUVMSchema(self, data):
        query = '''insert into [UVM].[schema] values(?)'''
        values = (data['Version'])
        cursor.execute(query, values)
        conn.commit()

    def insertUVMAggregrationDiscrepencies(self, data):
        query = '''insert into UVM.aggregations_discrepencies values(?,?,?,?,?,?,?,?,?,?)'''
        values = (data['CVE'], data['ComputerID'],data['SiteName'],data['FixletID'],
                  data['OperatingSystem'],data['CVSS3'],data['CVSS3Severity'],data['DatePublished'],data['FirstDetected'],
                  data['WeightedScore'])
        cursor.execute(query, values)
        conn.commit()

    def insertUVMAggregationsFindings(self, data):
        query = '''insert into UVM.aggregations_findings values (?,?,?,?,?,?,?,?,?,?,?)'''
        values = (data['CVE'], data['ComputerID'], data['ComputerGroup'], data['SiteName'], data['FixletID'],
                  data['FirstDetected'], data['OperatingSystem'], data['CVSS3'], data['CVSS3Severity'],
                  data['WeightedScore'], data['ApplicationComputers'])
        cursor.execute(query, values)
        conn.commit()

    def insertUVMAggregationsVulnerabilities(self, data):
        query = '''insert into UVM.aggregations_vulnerabilities values(?,?,?,?,?,?,?,?)'''
        values = (
        data['CVE'], data['SiteName'], data['FixletID'], data['DatePublished'], data['CVSS3'], data['CVSS3Severity'],
        data['WeightedScore'], data['ApplicationComputers'])
        cursor.execute(query, values)
        conn.commit()

