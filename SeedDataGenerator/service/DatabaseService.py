from SeedDataGenerator.models.InsertDatabase import InsertDatabase
from SeedDataGenerator.models.GetFromDatabase import GetFromDatabase
from faker import Faker
import random
from pyodbc import IntegrityError
import datetime

class DatabaseService:
    fake = Faker()
    insertTable = InsertDatabase()
    getData = GetFromDatabase()

    def devUVMAssessements(self):
        i = 1
        while i < 1000:
            dataDict = {
                "AssessmentID": i,
                "Title": "Vulnerability" + " " + str(self.fake.random_int(5)),
                "ScanType": random.choice(("Detection", "False Positive")),
                "DatePerformed": self.fake.date_time(),
                "PerformedBy": random.choice(("Qualys", "Rapid7", "AppScan")),
                "duration": random.choice(("00:02:01", "00:03:23", "00:04:12"))
            }
            self.insertTable.insertUVM_Assessements(dataDict)
            i += 1

    def devUVMVulnerabilites(self):
        i = 0
        while i < 1000:
            dataDict = {
                "CVE": "CVE" + str(self.fake.random_number(3)) + "-" + str(self.fake.random_number(3)) + "-" + str(
                    self.fake.random_number(3)),
                "Title": "Vulnerability" + " " + str(self.fake.random_int(5)),
                "Type": random.choice(("Detection", "False Positive")),
                "Severity": random.choice(("Critical", "High", "Medium", "Low", "None")),
                "CVSS_base": random.choice(("Critical", "High", "Medium", "Low", "None")),
                "CVSS_temporal": random.choice(("Critical", "High", "Medium", "Low", "None")),
                "CVSS3_base": random.choice(("Critical", "High", "Medium", "Low", "None")),
                "CVSS3_temporal": random.choice(("Critical", "High", "Medium", "Low", "None")),
                "Impact": random.choice(("Critical", "High", "Medium", "Low", "None")),
                "Solution": random.choice(("Compliance", "IT", "Cloud Security", "DEVOPS", "Web App Security")),
                "Category": random.choice(("Urgent", "Critical", "Serious", "Medium", "Minimal")),
                "Description": random.choice(("For enterprises", "Zion Security", "E-Spin Group Partners",
                                              "Forty Your Organization Resistance to Cyber Threats",
                                              "Everything need to know about Vulnerability Management")),
                "DatePublished": self.fake.date()

            }
            self.insertTable.insertUVM_Vulnerabilites(dataDict)
            i += 1

    def devUVMAssessmentTargets(self):
        i = 1
        while i < 1000:
            dataDict = {
                "AssessmentID": i,
                "ComputerID": self.fake.random_number(5),
            }
            self.insertTable.insertUVMAssessmentTargets(dataDict)
            i += 1

    def devUVMFindings(self):
        dataList = []
        computerID = self.getData.getDataUVMAssessmentTargets()
        CVE = self.getData.getDataUVMVlnerabilites()
        # list compression
        for idx, row in enumerate(computerID):
            dataList.append({
                "ComputerID": row[0],
                "CVE": CVE[idx][0],
                "FirstDetected": self.fake.date_time(),
                "LastDetected": self.fake.date_time()
            })
        i = 0
        try:
            for data in dataList:
                self.insertTable.insertUVMFindings(data)
                i += 1
        except IntegrityError:
            print(f"{i} numbers of row has been inserted into to database")

    def devUVMVulnerabilityFixletNexus(self):
        dataCVE = self.getData.getDataUVMVlnerabilites()
        for row in dataCVE:
            dataDict = {
                "CVE": row[0],
                "SiteName": random.choice(("Patches for Windows", "Patches for RHEL")),
                "FixletID": self.fake.random_number(5)
            }
            self.insertTable.insertUVMVulnerabilityFixletNexus(dataDict)

    def devUVMAssessementFindingNexus(self):
        i = 1
        computerID = self.getData.getDataUVMFindings()
        for row in computerID:
            dataDict = {
                "AssessmentID": i,
                "ComputerID": row[0]
            }
            i += 1
            self.insertTable.insertUVMAssessementFindingNexus(dataDict)

    def devUVMAssessementVulnerabilitesNexus(self):
        dataList = []
        assessmentID = self.getData.getDataUVMAssessment()
        dataCVE = self.getData.getDataUVMVlnerabilites()
        for idx, rowAssess in enumerate(assessmentID):
            dataList.append({
                "CVE": dataCVE[idx][0],
                "AssessmentID": rowAssess[0]
            })
        for data in dataList:
            self.insertTable.insertUVMAssessementVulnerabilitesNexus(data)

    def devUVMSchema(self):
        i = 0
        while i < 1000:
            dataDict = {
                "Version": str(i) + "." + str(0) + "." + str(i + 2)
            }
            self.insertTable.insertUVMSchema(dataDict)
            i += 1

    def devUVMAggregationsDiscrepencies(self):
        i = 0
        while i < 1000:
            dataDict = {
                "CVE": "CVE" + str(self.fake.random_number(3)) + "-" + str(self.fake.random_number(3)) + "-" + str(
                    self.fake.random_number(3)),
                "ComputerID": self.fake.random_number(5),
                "SiteName": random.choice(("Patches for Windows", "Patches for RHEL")),
                "FixletID": self.fake.random_number(3),
                "OperatingSystem": random.choice(("Linux", "Windows", "macOS")),
                "CVSS3": str(self.fake.random_number(1)) + "." + str(self.fake.random_number(2)),
                "CVSS3Severity": random.choice(("Critical", "High", "Medium", "Low", "None")),
                "DatePublished": self.fake.date(),
                "FirstDetected": self.fake.date_time(),
                "WeightedScore": self.fake.random_number(4)
            }
            self.insertTable.insertUVMAggregrationDiscrepencies(dataDict)
            i += 1

    def devUVMAggregationsFindings(self):
        i = 0
        while i < 1000:
            dataDict = {
                "CVE": "CVE" + str(self.fake.random_number(3)) + "-" + str(self.fake.random_number(3)) + "-" + str(
                    self.fake.random_number(3)),
                "ComputerID": self.fake.random_number(5),
                "ComputerGroup": random.choice(("Computers", "Vulnerabilites", "Child Groups")),
                "SiteName": random.choice(("Patches for Windows", "Patches for RHEL")),
                "FixletID": self.fake.random_number(3),
                "FirstDetected": self.fake.date_time(),
                "OperatingSystem": random.choice(("Linux", "Windows", "macOS")),
                "CVSS3": str(self.fake.random_number(1)) + "." + str(self.fake.random_number(2)),
                "CVSS3Severity": random.choice(("Critical", "High", "Medium", "Low", "None")),
                "WeightedScore": self.fake.random_number(4),
                "ApplicationComputers": self.fake.random_number(4)
            }
            self.insertTable.insertUVMAggregationsFindings(dataDict)
            i += 1

    def devUVMAggregationsVulnerabilites(self):
        i = 0
        while i < 1000:
            dataDict = {
                "CVE": "CVE" + str(self.fake.random_number(3)) + "-" + str(self.fake.random_number(3)) + "-" + str(
                    self.fake.random_number(3)),
                "SiteName": random.choice(("Patches for Windows", "Patches for RHEL")),
                "FixletID": self.fake.random_number(3),
                "DatePublished": self.fake.date_time(),
                "CVSS3": str(self.fake.random_number(1)) + "." + str(self.fake.random_number(2)),
                "CVSS3Severity": random.choice(("Critical", "High", "Medium", "Low", "None")),
                "WeightedScore": self.fake.random_number(4),
                "ApplicationComputers": self.fake.random_number(4)
            }
            self.insertTable.insertUVMAggregationsVulnerabilities(dataDict) # mock class
            i += 1
